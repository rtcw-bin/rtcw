﻿
![picture](https://cdn2.steamgriddb.com/logo_thumb/1b36ea1c9b7a1c3ad668b8bb5df7963f.png)

Return to Castle Wolfenstein repository for repackaging the game and the iortcw binaries, this is also a set of AUR packages.

All binaries are compiled from the official project/repository
https://github.com/iortcw/iortcw

Packages: 
[iortcw-bin](https://aur.archlinux.org/packages/iortcw-bin))
[rtcw](https://aur.archlinux.org/packages/rtcw))

 ### Author
  * Corey Bruce
